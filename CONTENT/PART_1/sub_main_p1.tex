\section{Assumption, model and definitions}
\input{CONTENT/DECORUM/current_toc}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
  \frametitle{Spectral unmixing problem}
  \begin{block}{Hyperspectral imaging}
      Let us consider a scene,
      \begin{itemize}
        \item Extracts information about the \textbf{present electromagnetic spectra} over a \textbf{large range of spectral bands};
        %\item This scene may be composed of \textbf{many materials in variable proportions};
        \item A pixel \textcolor{battleshipgrey}{$\Rightarrow$} the observed reflectance spectrum is a \textbf{mixture} formed by \textbf{different contributions} of the materials \textbf{spectral signatures}.
      \end{itemize}
  \end{block}
  \begin{exampleblock}{Spectral unmixing problem (SU)}
    \begin{itemize}
      \item A \textbf{blind source separation} problem \ie estimate both \textbf{spectral signatures} - aka. \textit{atoms} or \textit{endmembers} - and \textbf{their proportions} aka. \textit{abundances};
      %\item \textbf{Our objective :} The determination of the \textbf{present pure spectral signatures} - aka. \textit{atoms} or \textit{endmembers} - in the scene and \textbf{their proportions} - aka. \textit{abundances};
      \item \textbf{Supervised SU :} the mixture is searched in a known \textbf{dictionary} of reference spectra.
    \end{itemize}
  \end{exampleblock}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
  \frametitle{Linear Mixing Model \citep{singer1979mars}}
  \begin{block}{Assumption : Linear Mixing Model (LMM)}
      The \textbf{observed mixture} for a pixel can be expressed as the \textbf{linear combination of endmembers weighted by the fractional} abundances of each atom.
  \end{block}
  \vspace*{0.1cm}
  \begin{columns}
    \begin{column}{0.4\textwidth}  %%<--- here
      \scalebox{0.75}{
      \input{CONTENT/PART_1/mixing}
      }
    \end{column}


  \begin{column}{0.6\textwidth}
    \textcolor{battleshipgrey}{$\Rightarrow$} For a given pixel observed on $\Nl$ spectral bands :
    \begin{equation*}
    	\vy = \mH\vx + \ve\ms \Leftrightarrow \ms \vy = \sum_{q \in  \llbracket 1,Q\rrbracket} \xq\vhq + \ve%, \quad \forall p \in \llbracket 1,\Np \rrbracket
      \label{eq:LMM}
    \end{equation*}
    %\vspace*{-0.5cm}
    where
    \begin{description}
      \item[$\vy \in \Rn$] : the reflectance spectrum;
      \item[$\mH \in \Rnq$] : the dictionary of $Q$ atoms;
      \item[$\vx \in \Rq$] : \textcolor{ao}{the abundance vector};
      \item[$\ve \in \Rn$] : additional noise vector;
      \item[$\vhq$,$\xq$ ] : the $q$-th atom and its abundance.
    \end{description}
  \end{column}

  \end{columns}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
  \frametitle{Sum-to-one \& nonnegativity constraints}
  \begin{equation*}
    \vy = \sum_{q \in \llbracket 1,Q\rrbracket} \xq\vhq + \ve %\quad \forall p \in \llbracket 1,\Np \rrbracket
  	\tag{LMM}
  \end{equation*}
  \vspace*{-0.15cm}
  \begin{block}{Motivation :}
    \begin{itemize}
        \item To express these abundances $\vx$ as percentages;
        \item An active endmember \textcolor{battleshipgrey}{$\Rightarrow$} a nonnegative explanatory contribution to the observed spectrum;
        \item The reflectance spectrum is only a part of the emitted light (absorption by materials) \textcolor{battleshipgrey}{$\sim$} Normalization of the observed abundances.
        %\item \textbf{Enforce the sparsity of the model}.
    \end{itemize}
  \end{block}
  \vspace*{0.15cm}
  \begin{columns}
  \begin{column}{0.45\textwidth}
    \begin{block}{Abundance nonnegativity constraint}
        \begin{center}
            $\vx\geq 0 \Longleftrightarrow \xq \geq 0 \ms \forall \q \in \llbracket 1,Q\rrbracket$
        \end{center}
    \end{block}
  \end{column}
  \begin{column}{0.45\textwidth}  %%<--- here
    \begin{block}{Abundance sum-to-one constraint}
      \begin{center}
          ${\vo}_{Q}^{\intercal}\vx =1 \Longleftrightarrow \sum_{q \in \llbracket 1,Q\rrbracket} \xq = 1 $
      \end{center}
    \end{block}
  \end{column}
  \end{columns}
  \begin{exampleblock}{Remark : }
      \begin{itemize}
        \item This SU formulation is well identified in the literature as \textbf{Fully Constrained Least Squares} problems \citep{heinz2001fully} which naturally produce solutions where \textbf{some abundances are zero}.
        \item in \textbf{complex problems}, the estimated abundances are \textbf{spread over a substantial number} of components.
      \end{itemize}
  \end{exampleblock}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
  \frametitle{Sparsity constraint}
  \begin{block}{Motivations}
    \begin{itemize}
      \item \textbf{Physical constraint} : the observed scene is composed of a \textbf{limited number} of materials;
      \item The FCLS formulation may \textbf{fail in locating} the true endmembers;
      \item \textbf{Enforcing the number of nonzero abundances} may merely \textbf{ensure a correct mixture interpretation}.
    \end{itemize}
  \end{block}
  \begin{block}{The support of $\vx$}
      \begin{center}
          $\supp(\vx) := \{q \in \llbracket 1,Q\rrbracket| \xq \neq 0\}$
      \end{center}
  \end{block}

  \begin{block}{The counting function : the $\ell_{0}$-"norm"}
      \begin{center}
          $\|\vx\|_{0} := \card\left(\supp(\vx)\right)$
      \end{center}
  \end{block}

  \begin{block}{$\Kn$-sparse solution}
      \begin{center}
          $\vx \in \Rq\ms\st\ms\|\vx\|_{0} \leq \Kn,\ms \Kn \in\mathbb{Z}_{+}^{\ast}$
      \end{center}
  \end{block}
  % \begin{alertblock}{Misnomer}
  %   $\|.\|_{0}$ is \textbf{not a proper norm} : $\|\lambda \vx\|_{0} \neq |\lambda|\cdot\|\vx\|_{0}$ $\forall \lambda \in \R^{*}\setminus\{1\}$ (homogeneity property)
  % \end{alertblock}
\end{frame}

\begin{frame}
  \frametitle{Sparse spectral unmixing problem (\LZSU)}
  \begin{equation}
  	\begin{aligned}
  			\min_{{\vx} \in [0,1]^{Q}} \quad & \frac{1}{2}\big\|\vy-\mH\vx\big\|_{2}^{2}\\
  			%\st \quad & \|\vx\|_{0} \leq \Kh, \quad \|\vx\|_{1} \leq 1 %\\
  			\st \quad & \|\vx\|_{0} \leq \Kn, \quad {\vo}_{Q}^{\intercal}\vx =1 %\\
  	\end{aligned}
  	%\quad\forall p \in \llbracket 1,\Np \rrbracket
  	\tag{$\PTZ$}
  	\label{eq:PTZ}
  \end{equation}
  where $\Kn\in\mathbb{Z}_{+}^{\ast}$ the sparsity coefficient \textbf{set \textit{a priori}}.
  \begin{alertblock}{It's a complex problem}
    \begin{itemize}
      \item $\ell_{0}$-"norm" is not differentiable, not continuous \textcolor{battleshipgrey}{$\Rightarrow\ms$}$\PTZ$ is \textbf{not convex};
      \item $\ell_{0}$-"norm" minimization problems are $\mathcal{NP}$-Hard \citep{natarajan_sparse_1995};
      \item Can be \textbf{only considered} for problems with a \textbf{limited amount of atoms}.
    \end{itemize}
  \end{alertblock}
  \begin{exampleblock}{But in practice}
    \begin{itemize}
      \item The \textbf{number of active atoms} in a mixture is relatively \textbf{small} \eg $\Kn \in \llbracket 1,6 \rrbracket$;
      %\item The reflectance spectra are measured is of the order of a few hundred;
      \item The \textbf{dictionary} is typically composed of $10\sim 500$ spectral signatures;
      \item The \textbf{sum-to-one \& nonnegativity constraints restrict} the space of feasible solutions.
    \end{itemize}
  \end{exampleblock}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
  \frametitle{Exact optimisation of the \LZSU}
  \begin{block}{A short state of the art}
      \begin{description}
        \item[Approximation:] greedy algorithms using heuristics, convex or nonconvex relaxation \citep{tropp_computational_2010}
        \begin{itemize}
            \item \textbf{Reduce} the complexity of an \textbf{exhaustive exploration};
            \item \textbf{Rapidly} provide sparse solution;
            \item The obtained results are \textbf{often suboptimal}.
        \end{itemize}
        \item[Standard $\ell_{0}$ problems using MIP techniques:]\textcolor{UBCwhite}{.}\newline \citep{Bourguignon2016}, \citep{Bertsimas2016};
        \item[Dedicated branch-and-bound algorithm to tackle $\ell_{0}$ problems:]\textcolor{UBCwhite}{.}\newline \citep{Bienstock1996},\citep{Bertsimas2009}, \citep{RamziPhd}, \citep{hazimeh2021sparse};
      \end{description}
  \end{block}

  \begin{exampleblock}{Our approach}
      Implementing an \textbf{exact optimization algorithm} for the \LZSUs %\textcolor{battleshipgrey}{$\Rightarrow$} Branch-and-bound algorithm.
  \end{exampleblock}

\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
  \frametitle{Exact optimisation of the \LZSUs as a MIP \citep{RBMWHISPERS}}
  \begin{equation}
    \begin{aligned}
        \min_{{\vx} \in [0,1]^{Q}} \quad & \frac{1}{2}\big\|\vy-\mH\vx\big\|_{2}^{2}\\
        %\st \quad & \|\vx\|_{0} \leq \Kh, \quad \|\vx\|_{1} \leq 1 %\\
        \st \quad & \|\vx\|_{0} \leq \Kn, \quad {\vo}_{Q}^{\intercal}\vx =1 %\\
    \end{aligned}
    %\quad\forall p \in \llbracket 1,\Np \rrbracket
    \tag{$\PTZ$}
    \label{eq:PTZ}
  \end{equation}

  \begin{block}{MIP formulation}
    \begin{description}
        \item[$\quad$Introduce a binary decision variables:] $\vb \in \{0,1\}^{Q}$ $\st$ $\xq = 0 \Leftrightarrow {\bi}_{q} = 0 \ms \forall q \in \llbracket 1,Q\rrbracket$;
        \item[$\quad$Link $\vx$ and $\vb$:] $|\vx| \leq M\vb$ with $M\in\mathbb{R}_{+}^{*}$ aka. \textit{bigM assumption};\newline
                                          \textcolor{cadmiumred}{\textbf{Trivially}} with $\vx \in [0,1]$ : $M=1$ and $\vx\leq\vb$
        \item[$\quad$Rewrite the $\ell_{0}$-"norm":] $\|{\vx}\|_{0} = \sum_{q \in \llbracket 1,Q\rrbracket} {\bi}_{q}$
    \end{description}
  \end{block}

  \begin{equation}
  	\begin{aligned}
  			\min_{\substack{\vx \in [0,1]^{Q}\\\textcolor{ao}{\boldsymbol{b} \in \{0,1\}^{Q}}}} \quad & \frac{1}{2}\big\|{\vy}-{\mH}{\vx}\big\|_{2}^{2} \\
  			\st \quad & \sum_{q \in \llbracket 1,Q\rrbracket} \textcolor{ao}{{\bi}_{q}} \leq \Kn ,\quad {\vo}_{Q}^{\intercal}\vx =1,\quad \vx \leq \textcolor{ao}{\vb}
  	\end{aligned}
  	\tag{$\PTZ$}
  	%\tag{$\PhatTZ$}
    \label{eq:Phat2/0}
  \end{equation}
  \textcolor{battleshipgrey}{$\Rightarrow$} Just use IBM Cplex solver \dots

  % \begin{block}{A short state of the art II}
  %     \begin{description}
  %       %\item[Standard $\ell_{0}$ problems using MIP techniques:]\textcolor{UBCwhite}{.}\newline \citep{Bourguignon2016}, \citep{Bertsimas2016};
  %       %\item[Dedicated branch-and-bound algorithm to tackle $\ell_{0}$ problems:]\textcolor{UBCwhite}{.}\newline \citep{Bienstock1996},\citep{Bertsimas2009}, \citep{hazimeh2021sparse}, \citep{RamziPhd};
  %       \item[\LZSUs MIP formulation:]\textcolor{UBCwhite}{.}\newline \citep{RBMWHISPERS};
  %       \item[\LZSUs dedicated branch-and-bound:] $\lambda$ ?
  %     \end{description}
  % \end{block}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
  \frametitle{Exact optimisation of the \LZSUs - Our approach}
  \begin{exampleblock}{Our approach}
    \begin{itemize}
        \item Implementing a \textbf{dedicated} and \textbf{exact optimization algorithm} for the \LZSUs \\\textcolor{battleshipgrey}{$\Rightarrow$} Branch-and-bound algorithm;
        \item Try to be \textit{\cancel{harder}, \textcolor{ao}{better}, \textcolor{ao}{faster}, \textcolor{ao}{stronger}} than a commercial solver \citep{RamziPhd};
        \item Provide an \textbf{open-source} software to the SU community.
    \end{itemize}

  \end{exampleblock}
\end{frame}
